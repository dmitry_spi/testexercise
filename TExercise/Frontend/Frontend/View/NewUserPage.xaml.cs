﻿using Frontend.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewUserPage : ContentPage
	{
		public NewUserPage(ICommand refreshCommand)
		{
			InitializeComponent ();
            this.BindingContext = new NewUserViewModel(refreshCommand, this) { Navigation = Navigation };
		}
	}
}