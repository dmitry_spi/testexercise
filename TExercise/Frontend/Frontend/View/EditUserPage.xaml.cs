﻿using Frontend.Model;
using Frontend.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditUserPage : ContentPage
	{
		public EditUserPage(AllUsersResponse userModel, ICommand refreshCommand)
		{
			InitializeComponent ();
            BindingContext = new EditUserViewModel(userModel, refreshCommand, this) { Navigation = Navigation };
		}
	}
}