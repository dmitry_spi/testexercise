﻿using Frontend.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Goods : ContentPage
	{
		public Goods()
		{
			InitializeComponent();

            this.BindingContext = new GoodsViewModel();
		}
	}
}