﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View.Components
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SectionView : ContentView
	{
		public SectionView(ImageSource imageSource, string sectionTitle, Page sectionPage)
		{
			InitializeComponent();
            SectionImage.Source = imageSource;
            SectionTitle.Text = sectionTitle;
            var Tap = new TapGestureRecognizer();
            Tap.Tapped += (s, e) =>
            {
                Navigation.PushAsync(sectionPage);
            };
            Section.GestureRecognizers.Add(Tap);
		}
	}
}