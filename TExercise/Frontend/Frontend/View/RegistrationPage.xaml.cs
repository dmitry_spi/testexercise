﻿using Frontend.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationPage : ContentPage
	{
		public RegistrationPage(ICommand refreshCommand)
		{
			InitializeComponent();
            this.BindingContext = new RegistrationViewModel(this, refreshCommand) { Navigation = Navigation };
        }
	}
}