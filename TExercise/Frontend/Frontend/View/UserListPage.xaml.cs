﻿using Frontend.Model;
using Frontend.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserListPage : ContentPage
	{
		public UserListPage ()
		{
            //ObservableCollection<AllUsersResponse> UserListCollection = new ObservableCollection<AllUsersResponse> {new AllUsersResponse() };
            InitializeComponent();
            UsersListView.BindingContext = BindingContext = new UserListViewModel(this) {Navigation=Navigation };
            UsersListView.ItemTapped += (s, e) =>
            {
                if (e.Item == null)
                    return;
                ((ListView)s).SelectedItem = null;
            };
        }
	}
}