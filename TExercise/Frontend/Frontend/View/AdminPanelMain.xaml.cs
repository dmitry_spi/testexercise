﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AdminPanelMain : ContentPage
	{
		public AdminPanelMain ()
		{
			InitializeComponent();
            ContentButton.Clicked += (s, e) =>
            {
                Navigation.PushAsync(new AdminContentPage());
            };
            UMButton.Clicked += (s, e) =>
            {
                Navigation.PushAsync(new UserListPage());
            };
		}
	}
}