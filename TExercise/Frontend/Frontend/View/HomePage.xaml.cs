﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage()
		{
			InitializeComponent();
            AddSections();
		}

        private void AddSections()
        {
            List<string> titles = new List<string>()
            {
                "Materials",
                "Tools",
                "Food"
            };
            for (int i = 0; i<3; i++)
            {
                var sectionView = new Components.SectionView(ImageSource.FromResource("Frontend.Res.section_img.jpg"),titles[i].ToUpper(),new Goods());
                HomeStack.Children.Add(sectionView);
            }
        }
	}
}