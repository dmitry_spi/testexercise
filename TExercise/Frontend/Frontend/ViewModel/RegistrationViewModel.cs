﻿using Frontend.General;
using Frontend.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Frontend.ViewModel
{
    class RegistrationViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand RegCommand { protected set; get; }
        public RegistrationRequest RegModel { get; set; }
        private bool _process;
        private bool _regbutvisible = true;
        private Page _page;
        public INavigation Navigation;
        private ICommand _refreshCommand;

        public RegistrationViewModel(Page page, ICommand refreshCommand)
        {
            _page = page;
            _refreshCommand = refreshCommand;
            RegModel = new RegistrationRequest();
            RegCommand = new Command(Registration);
        }

        public string Username
        {
            get
            {
                return RegModel.Username;
            }
            set
            {
                if (RegModel.Username != value)
                {
                    RegModel.Username = value;
                    OnPropertyChanged("Username");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public string Email
        {
            get
            {
                return RegModel.Email;
            }
            set
            {
                if (RegModel.Email != value)
                {
                    RegModel.Email = value;
                    OnPropertyChanged("Email");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public string Password
        {
            get
            {
                return RegModel.Password;
            }
            set
            {
                if (RegModel.Password != value)
                {
                    RegModel.Password = value;
                    OnPropertyChanged("Password");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public string RepeatPassword
        {
            get
            {
                return RegModel.ComfPassword;
            }
            set
            {
                if (RegModel.ComfPassword != value)
                {
                    RegModel.ComfPassword = value;
                    OnPropertyChanged("RepeatPassword");
                    OnPropertyChanged("RepeatPasswordColor");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public Color RepeatPasswordColor
        {
            get
            {
                if (Password == RepeatPassword)
                {
                    return Color.Black;
                }
                else
                {
                    return Color.Red;
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password) 
                    && !string.IsNullOrEmpty(RepeatPassword) && !string.IsNullOrEmpty(Email) 
                    && Password.CompareTo(RepeatPassword)==0);
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    RegButVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool RegButVisible
        {
            get
            {
                return _regbutvisible;
            }
            set
            {
                if (_regbutvisible != value)
                {
                    _regbutvisible = value;
                    OnPropertyChanged("RegButVisible");
                }
            }
        }

        private async void Registration()
        {
            if (!IsValid)
            {
                await _page.DisplayAlert("Error", "All fields must be filled", "OK");
                return;
            }
            Process = true;
            var user_manager = new UserManager();
            var result = await user_manager.Registration(RegModel);
            if (result == -1)
            {
                await _page.DisplayAlert("Error", "Something wrong", "OK");
            }
            else
            {
                _refreshCommand.Execute(null);
                await Navigation.PopAsync();
            }
            Process = false;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
