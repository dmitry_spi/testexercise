﻿using Frontend.General;
using Frontend.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Frontend.ViewModel
{
    class LoginViewModel : INotifyPropertyChanged
    {
        public ICommand LoginCommand { protected set; get; }
        public event PropertyChangedEventHandler PropertyChanged;
        public LoginRequest LoginModel { get; set; }
        private bool _process;
        private bool _loginbutvisible = true;
        private Page _page;
        public INavigation Navigation;
        private ICommand _refreshCommand;

        public LoginViewModel(Page page, ICommand refreshCommand)
        {
            _page = page;
            _refreshCommand = refreshCommand;
            LoginModel = new LoginRequest();
            LoginCommand = new Command(Login);
        }

        public string Username
        {
            get
            {
                return LoginModel.Username;
            }
            set
            {
                if (LoginModel.Username != value)
                {
                    LoginModel.Username = value;
                    OnPropertyChanged("Username");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public string Password
        {
            get
            {
                return LoginModel.Password;
            }
            set
            {
                if (LoginModel.Password != value)
                {
                    LoginModel.Password = value;
                    OnPropertyChanged("Password");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password));
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    LoginButVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool LoginButVisible
        {
            get
            {
                return _loginbutvisible;
            }
            set
            {
                if (_loginbutvisible != value)
                {
                    _loginbutvisible = value;
                    OnPropertyChanged("LoginButVisible");
                }
            }
        }

        private async void Login()
        {
            if (!IsValid)
            {
                return;
            }
            Process = true;
            var user_manager = new UserManager();
            var result = await user_manager.Login(LoginModel, false);
            if (result == -1)
            {
                await _page.DisplayAlert("Error", "Bad request", "OK");
            }
            if (result == 0)
            {
                await _page.DisplayAlert("Error", "Wrong login or/and password", "OK");
            }
            if (result == 1)
            {
                _refreshCommand.Execute(null);
                await Navigation.PopAsync();
            }
            Process = false;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
