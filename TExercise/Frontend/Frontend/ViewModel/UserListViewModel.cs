﻿using Frontend.General;
using Frontend.Model;
using Frontend.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Frontend.ViewModel
{
    class UserListViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation;
        private List<UserListModel> _usersList;
        private List<UserListModel> _filteredUsersList;
        private List<string> _roles;
        private AdminControl adminControl = new AdminControl();
        private UserManager userManager = new UserManager();
        private string _selectedrole="none";
        private bool _process;
        private bool _elemvisible;
        public ICommand LoadCommand { protected set; get; }
        public ICommand LoadUsersCommand { protected set; get; }
        public ICommand CreateCommand { protected set; get; }
        private Page _page;

        public UserListViewModel(Page page)
        {
            _page = page;
            LoadUsersCommand = new Command(async()=>await GetAllUsers());
            CreateCommand = new Command(OpenCreatePage);
            LoadCommand = new Command(LoadAll);
            LoadCommand.Execute(null);
        }

        public List<UserListModel> UsersList
        {
            get
            {
                FilterUserList();
                return _filteredUsersList;
            }
            set
            {
                _usersList = value;
                OnPropertyChanged("UsersList");
            }
        }

        public List<string> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
                OnPropertyChanged("Roles");
            }
        }

        public string SelectedRole
        {
            get
            {
                return _selectedrole;
            }
            set
            {
                if (_selectedrole != value)
                {
                    _selectedrole = value;
                    OnPropertyChanged("SelectedRole");
                    OnPropertyChanged("UsersList");
                }
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    ElemVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool ElemVisible
        {
            get
            {
                return _elemvisible;
            }
            set
            {
                if (_elemvisible != value)
                {
                    _elemvisible = value;
                    OnPropertyChanged("ElemVisible");
                }
            }
        }

        public bool CreateButtonAcces
        {
            get
            {
                if (userManager.RoleAP.CompareTo("Admin") == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private async void LoadAll()
        {
            Process = true;
            if (await userManager.RefreshUserToken(true))
            {
                await GetAllRoles();
                await GetAllUsers();
            }
            else
            {
                await Navigation.PopToRootAsync();
            }
            Process = false;
        }

        private async Task GetAllUsers()
        {
            Process = true;
            var list = await adminControl.GetAllUsers();
            var user_list = new List<UserListModel>();
            int i = 0;
            foreach (var item in list)
            {
                user_list.Add(new UserListModel
                {
                    Id = i,
                    Username = item.Username,
                    Name = item.Name,
                    Role = item.Role,
                    ButtonsEnabled = CheckUser(item),
                    EditCommand = new Command(()=> {
                        OpenEditPage(item);
                    }),
                    DeleteCommand = new Command(() => {
                        DeleteUser(item);
                    })
                });
                i++;
            }
            UsersList = user_list;
            Process = false;
        }

        private bool CheckUser(AllUsersResponse allUsersResponse)
        {
            if (userManager.RoleAP.CompareTo("Admin") == 0)
            {
                return true;
            }
            else if (userManager.RoleAP.CompareTo("Moderator") == 0)
            {
                if (userManager.UsernameAP.CompareTo(allUsersResponse.Username) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        private async Task GetAllRoles()
        {
            Process = true;
            Roles = await adminControl.GetAllUsersRoles();
            Process = false;
        }

        private void FilterUserList()
        {
            if (_usersList!=null)
            {
                var list = new List<UserListModel>();
                foreach (var item in _usersList)
                {
                    if (item.Role == SelectedRole)
                    {
                        list.Add(item);
                    }
                }
                _filteredUsersList = list;
            }
        }

        private async void OpenEditPage(AllUsersResponse user)
        {
            await Navigation.PushAsync(new EditUserPage(user,LoadUsersCommand));
        }

        private async void OpenCreatePage()
        {
            await Navigation.PushAsync(new NewUserPage(LoadUsersCommand));
        }

        private async void DeleteUser(AllUsersResponse user)
        {
            Process = true;
            var delete_user = new DeleteUserRequest
            {
                Username = user.Username
            };
            if (await adminControl.DeleteUser(delete_user) == 1)
            {
                LoadUsersCommand.Execute(null);
                await _page.DisplayAlert("OK", "Completed", "OK");
            }
            else
            {
                await _page.DisplayAlert("Error", "Something wrong", "OK");
            }
            Process = false;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }

    class UserListModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool ButtonsEnabled { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
    }
}
