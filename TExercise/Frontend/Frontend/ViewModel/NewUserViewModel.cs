﻿using Frontend.General;
using Frontend.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Frontend.ViewModel
{
    class NewUserViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private NewUserRequest _createUser = new NewUserRequest();
        private List<string> _rolesList;
        public INavigation Navigation;
        private AdminControl adminControl = new AdminControl();
        private UserManager userManager = new UserManager();
        private bool _process;
        private bool _elemVisible;
        public ICommand SaveChangesCommand { protected set; get; }
        public ICommand CancelCommand { protected set; get; }
        public ICommand LoadRolesCommand { protected set; get; }
        public ICommand _refreshCommand;
        private Page _page;

        public NewUserViewModel(ICommand refreshCommand, Page page)
        {
            _page = page;
            _refreshCommand = refreshCommand;
            SaveChangesCommand = new Command(SaveChanges);
            CancelCommand = new Command(Cancel);
            LoadRolesCommand = new Command(GetAllRoles);
            LoadRolesCommand.Execute(null);
        }

        public string Username
        {
            get
            {
                return _createUser.Username;
            }
            set
            {
                if (_createUser.Username != value)
                {
                    _createUser.Username = value;
                    OnPropertyChanged("Username");
                }
            }
        }

        public string Name
        {
            get
            {
                return _createUser.Name;
            }
            set
            {
                if (_createUser.Name != value)
                {
                    _createUser.Name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string Password
        {
            get
            {
                return _createUser.Password;
            }
            set
            {
                if (_createUser.Password != value)
                {
                    _createUser.Password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public string Email
        {
            get
            {
                return _createUser.Email;
            }
            set
            {
                if (_createUser.Email != value)
                {
                    _createUser.Email = value;
                    OnPropertyChanged("Email");
                }
            }
        }

        public string SelectedRole
        {
            get
            {
                return _createUser.Role;
            }
            set
            {
                if (_createUser.Role != value && value != null)
                {
                    _createUser.Role = value;
                }
                OnPropertyChanged("SelectedRole");
            }
        }

        public List<string> RolesList
        {
            get
            {
                return _rolesList;
            }
            set
            {
                _rolesList = value;
                OnPropertyChanged("RolesList");
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    ElemVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool ElemVisible
        {
            get
            {
                return _elemVisible;
            }
            set
            {
                if (_elemVisible != value)
                {
                    _elemVisible = value;
                    OnPropertyChanged("ElemVisible");
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return (!(String.IsNullOrWhiteSpace(_createUser.Username) ||
                String.IsNullOrWhiteSpace(_createUser.Name) ||
                String.IsNullOrWhiteSpace(_createUser.Password) ||
                String.IsNullOrWhiteSpace(_createUser.Role)));
            }
        }

        private async void SaveChanges()
        {
            if (!IsValid)
            {
                await _page.DisplayAlert("Error", "Fields* must be filled", "OK");
                return;
            }
            Process = true;
            if (await userManager.RefreshUserToken(true))
            {
                var result = await adminControl.NewUser(_createUser);
                if (result == 1)
                {
                    _refreshCommand.Execute(null);
                    await Navigation.PopAsync();
                }
                else
                {
                    await _page.DisplayAlert("Error", "Something wrong", "OK");
                }
            }
            else
            {
                await Navigation.PopToRootAsync();
            }
            Process = false;
        }

        private async void Cancel()
        {
            await Navigation.PopAsync();
        }

        private async void GetAllRoles()
        {
            Process = true;
            if (await userManager.RefreshUserToken(true))
            {
                RolesList = await adminControl.GetAllUsersRoles();
            }
            else
            {
                await Navigation.PopToRootAsync();
            }
            Process = false;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
