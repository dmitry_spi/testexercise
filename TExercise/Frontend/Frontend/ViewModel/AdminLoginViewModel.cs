﻿using Frontend.General;
using Frontend.Model;
using Frontend.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frontend.ViewModel
{
    class AdminLoginViewModel : INotifyPropertyChanged
    {
        public ICommand LoginCommand { protected set; get; }
        public event PropertyChangedEventHandler PropertyChanged;
        public LoginRequest LoginModel { get; set; }
        private bool _process;
        private bool _error = false;
        private bool _loginbutvisible = true;
        private bool _unsuccessful;
        public INavigation Navigation;

        public AdminLoginViewModel()
        {
            LoginModel = new LoginRequest();
            LoginCommand = new Command(Login);
        }

        public string Username
        {
            get
            {
                return LoginModel.Username;
            }
            set
            {
                if (LoginModel.Username != value)
                {
                    LoginModel.Username = value;
                    OnPropertyChanged("Username");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public string Password
        {
            get
            {
                return LoginModel.Password;
            }
            set
            {
                if (LoginModel.Password != value)
                {
                    LoginModel.Password = value;
                    OnPropertyChanged("Password");
                    OnPropertyChanged("IsValid");
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password));
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    LoginButVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool LoginButVisible
        {
            get
            {
                return _loginbutvisible;
            }
            set
            {
                if (_loginbutvisible != value)
                {
                    _loginbutvisible = value;
                    OnPropertyChanged("LoginButVisible");
                }
            }
        }

        public bool Error
        {
            get
            {
                return _error;
            }
            set
            {
                if (_error != value)
                {
                    _error = value;
                    OnPropertyChanged("Error");
                }
            }
        }

        public bool Unsuccessful
        {
            get
            {
                return _unsuccessful;
            }
            set
            {
                if (_unsuccessful != value)
                {
                    _unsuccessful = value;
                    OnPropertyChanged("Unsuccessful");
                }
            }
        }

        private async void Login()
        {
            if (IsValid)
            {
                Error = false;
                Unsuccessful = false;
                Process = true;
                var user_manager = new UserManager();
                var result = await user_manager.Login(LoginModel, true);
                Process = false;
                if (result == -1)
                {
                    Error = true;
                }
                if (result == 0)
                {
                    Unsuccessful = true;
                }
                if (result == 1)
                {
                    await Navigation.PushAsync(new AdminPanelMain());
                }
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
