﻿using Frontend.General;
using Frontend.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Frontend.ViewModel
{
    class EditUserViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private AllUsersResponse _userModel;
        private UpdateAccountRequest _updateUser = new UpdateAccountRequest();
        private List<string> _rolesList;
        public INavigation Navigation;
        private AdminControl adminControl = new AdminControl();
        private UserManager userManager = new UserManager();
        private bool _process;
        private bool _elemVisible;
        public ICommand SaveChangesCommand { protected set; get; }
        public ICommand CancelCommand { protected set; get; }
        public ICommand LoadRolesCommand { protected set; get; }
        public ICommand _refreshCommand;
        private Page _page;

        public EditUserViewModel(AllUsersResponse userModel, ICommand refreshCommand, Page page)
        {
            _page = page;
            _userModel = userModel;
            _updateUser.OldUsername = userModel.Username;
            _updateUser.NewUsername = userModel.Username;
            _updateUser.Name = userModel.Name;
            _updateUser.Role = userModel.Role;
            _refreshCommand = refreshCommand;
            SaveChangesCommand = new Command(SaveChanges);
            CancelCommand = new Command(Cancel);
            LoadRolesCommand = new Command(GetAllRoles);
            LoadRolesCommand.Execute(null);
        }

        public string Username
        {
            get
            {
                return _updateUser.NewUsername;
            }
            set
            {
                if (_updateUser.NewUsername != value)
                {
                    _updateUser.NewUsername = value;
                    OnPropertyChanged("Username");
                }
            }
        }

        public string Name
        {
            get
            {
                return _updateUser.Name;
            }
            set
            {
                if (_updateUser.Name != value)
                {
                    _updateUser.Name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string Password
        {
            get
            {
                return _updateUser.Password;
            }
            set
            {
                if (_updateUser.Password != value)
                {
                    _updateUser.Password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public string SelectedRole
        {
            get
            {
                return _updateUser.Role;
            }
            set
            {
                if (_updateUser.Role != value && value!=null)
                {
                    _updateUser.Role = value; 
                }
                OnPropertyChanged("SelectedRole");
            }
        }

        public bool SelectRoleEnabled
        {
            get
            {
                return CheckUser();
            }
        }

        public List<string> RolesList
        {
            get
            {
                return _rolesList;
            }
            set
            {
                _rolesList = value;
                OnPropertyChanged("RolesList");
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    ElemVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool ElemVisible
        {
            get
            {
                return _elemVisible;
            }
            set
            {
                if (_elemVisible != value)
                {
                    _elemVisible = value;
                    OnPropertyChanged("ElemVisible");
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return (!(String.IsNullOrWhiteSpace(_updateUser.NewUsername) ||
                String.IsNullOrWhiteSpace(_updateUser.Name) ||
                String.IsNullOrWhiteSpace(_updateUser.Role)));
            }
        }

        private async void SaveChanges()
        {
            if (!IsValid)
            {
                await _page.DisplayAlert("Error", "Fields* must be filled", "OK");
                return;
            }
            Process = true;
            if (await userManager.RefreshUserToken(true))
            {
                var result = await adminControl.UpdateUser(_updateUser);
                if (result == 1)
                {
                    _refreshCommand.Execute(null);
                    await Navigation.PopAsync();
                }
                else
                {
                    await _page.DisplayAlert("Error", "Something wrong", "OK");
                }
            }
            else
            {
                await Navigation.PopToRootAsync();
            }
            Process = false;
        }

        private async void Cancel()
        {
            await Navigation.PopAsync();
        }

        private async void GetAllRoles()
        {
            Process = true;
            if (await userManager.RefreshUserToken(true))
            {
                RolesList = await adminControl.GetAllUsersRoles();
            }
            else
            {
                await Navigation.PopToRootAsync();
            }
            Process = false;
        }

        private bool CheckUser()
        {
            if (userManager.RoleAP.CompareTo("Admin") == 0)
            {
                return true;
            }
            else if (userManager.RoleAP.CompareTo("Moderator") == 0)
            {
                return false;
            }
            return false;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
