﻿using Frontend.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Frontend.ViewModel
{
    class GoodsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private UserManager userManager = new UserManager();

        public bool CanBuy
        {
            get
            {
                return userManager.Role == "User";
            }
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
