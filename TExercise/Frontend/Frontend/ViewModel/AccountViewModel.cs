﻿using Frontend.General;
using Frontend.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Frontend.ViewModel
{
    class AccountViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand LoginCommand { protected set; get; }
        public ICommand RegCommand { protected set; get; }
        public ICommand ExitCommand { protected set; get; }
        public ICommand RefreshCommand { protected set; get; }

        public INavigation Navigation;

        private UserManager userManager = new UserManager();

        private bool _process;
        private bool _elemVisible = true;

        public AccountViewModel()
        {
            LoginCommand = new Command(OpenLoginPage);
            RegCommand = new Command(OpenRegPage);
            ExitCommand = new Command(Exit);
            RefreshCommand = new Command(RefereshData);
        }

        public string Username
        {
            get
            {
                return userManager.Username;
            }
        }

        public bool IsLogged
        {
            get
            {
                return userManager.RefreshToken != null;
            }
        }

        public bool IsNotLogged
        {
            get
            {
                return !IsLogged;
            }
        }

        public bool Process
        {
            get
            {
                return _process;
            }
            set
            {
                if (_process != value)
                {
                    _process = value;
                    ElemVisible = !value;
                    OnPropertyChanged("Process");
                }
            }
        }

        public bool ElemVisible
        {
            get
            {
                return _elemVisible;
            }
            set
            {
                if (_elemVisible != value)
                {
                    _elemVisible = value;
                    OnPropertyChanged("ElemVisible");
                }
            }
        }

        private async void OpenLoginPage()
        {
            await Navigation.PushAsync(new LoginPage(RefreshCommand));
        }

        private async void OpenRegPage()
        {
            await Navigation.PushAsync(new RegistrationPage(RefreshCommand));
        }

        private void RefereshData()
        {
            OnPropertyChanged("Username");
            OnPropertyChanged("IsLogged");
            OnPropertyChanged("IsNotLogged");
        }

        private async void Exit()
        {
            Process = true;
            if (await userManager.RefreshUserToken(false))
            {
                await userManager.SignOut(false);
            }
            OnPropertyChanged("Username");
            OnPropertyChanged("IsLogged");
            OnPropertyChanged("IsNotLogged");
            Process = false;
        }

        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
