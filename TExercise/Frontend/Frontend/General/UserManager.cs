﻿using Frontend.Model;
using Newtonsoft.Json;
using Plugin.SecureStorage;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Frontend.General
{
    class UserManager
    {
        public string Username
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("username");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("username", value);
            }
        }
        public string UsernameAP
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("username_ap");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("username_ap", value);
            }
        }
        public string Role
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("role");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("role", value);
            }
        }
        public string RoleAP
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("role_ap");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("role_ap", value);
            }
        }
        public string Name
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("name");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("name", value);
            }
        }
        public string NameAP
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("name_ap");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("name_ap", value);
            }
        }
        public string Token
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("token");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("token", value);
            }
        }
        public string TokenExpDate
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("token_exp");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("token_exp", value);
            }
        }
        public string RefreshToken
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("refresh_token");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("refresh_token", value);
            }
        }
        public string AdminToken
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("admin_token");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("admin_token", value);
            }
        }
        public string AdminTokenExpDate
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("admin_token_exp");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("admin_token_exp", value);
            }
        }
        public string AdminRefreshToken
        {
            get
            {
                return CrossSecureStorage.Current.GetValue("admin_refresh_token");
            }
            set
            {
                CrossSecureStorage.Current.SetValue("admin_refresh_token", value);
            }
        }

        private void DeleteUser()
        {
            CrossSecureStorage.Current.DeleteKey("username");
            CrossSecureStorage.Current.DeleteKey("role");
            CrossSecureStorage.Current.DeleteKey("name");
            CrossSecureStorage.Current.DeleteKey("token");
            CrossSecureStorage.Current.DeleteKey("token_exp");
            CrossSecureStorage.Current.DeleteKey("refresh_token");
        }

        private void DeleteUserAP()
        {
            CrossSecureStorage.Current.DeleteKey("username_ap");
            CrossSecureStorage.Current.DeleteKey("role_ap");
            CrossSecureStorage.Current.DeleteKey("name_ap");
            CrossSecureStorage.Current.DeleteKey("admin_token");
            CrossSecureStorage.Current.DeleteKey("admin_token_exp");
            CrossSecureStorage.Current.DeleteKey("admin_refresh_token");
        }

        public async Task<int> Login(LoginRequest loginModel, bool adminPanel) // -1 - BadRequest, 0 - Unauthorized, 1 - OK
        {
            var request = new RequestHandler();
            string result;
            if (adminPanel)
            {
                result = await request.MakeRequest<LoginRequest>("api/Admin/signin", null, loginModel);
            }
            else
            {
                result = await request.MakeRequest<LoginRequest>("api/Account/login", null, loginModel);
            }
            if (request.StatusCode == HttpStatusCode.OK)
            {
                var user_cred = JsonConvert.DeserializeObject<LoginResponse>(result);
                if (adminPanel)
                {
                    UsernameAP = user_cred.username;
                    NameAP = user_cred.name;
                    RoleAP = user_cred.user_role;
                    AdminToken = user_cred.acces_token;
                    AdminRefreshToken = user_cred.refresh_token;
                    AdminTokenExpDate = user_cred.exp_date.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                }
                else
                {
                    Username = user_cred.username;
                    Name = user_cred.name;
                    Role = user_cred.user_role;
                    Token = user_cred.acces_token;
                    RefreshToken = user_cred.refresh_token;
                    TokenExpDate = user_cred.exp_date.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                }
                return 1;
            }
            else if (request.StatusCode == HttpStatusCode.Unauthorized)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        public async Task<int> Registration(RegistrationRequest regModel) // -1 - BadRequest, 1 - OK
        {
            var request = new RequestHandler();
            var result = await request.MakeRequest<RegistrationRequest>("api/Account/registration", null, regModel);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                var user_cred = JsonConvert.DeserializeObject<LoginResponse>(result);
                Username = user_cred.username;
                Name = user_cred.name;
                Role = user_cred.user_role;
                Token = user_cred.acces_token;
                RefreshToken = user_cred.refresh_token;
                TokenExpDate = user_cred.exp_date.ToString();
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public async Task<bool> RefreshUserToken(bool adminPanel)
        {
            DateTime exp_date = DateTime.Parse(adminPanel ? AdminTokenExpDate : TokenExpDate).ToUniversalTime();
            if (DateTime.UtcNow > exp_date)
            {
                var token_refresh = new TokenRefreshRequest
                {
                    RefreshToken = adminPanel ? AdminRefreshToken : RefreshToken
                };
                var request = new RequestHandler();
                string result;
                if (adminPanel)
                {
                    result = await request.MakeRequest<TokenRefreshRequest>("api/Admin/refreshtkn", AdminRefreshToken, token_refresh);
                }
                else
                {
                    result = await request.MakeRequest<TokenRefreshRequest>("api/Account/refreshtoken", RefreshToken, token_refresh);
                }
                if (request.StatusCode == HttpStatusCode.OK)
                {
                    var user_cred = JsonConvert.DeserializeObject<LoginResponse>(result);
                    
                    if (adminPanel)
                    {
                        UsernameAP = user_cred.username;
                        NameAP = user_cred.name;
                        RoleAP = user_cred.user_role;
                        AdminToken = user_cred.acces_token;
                        AdminRefreshToken = user_cred.refresh_token;
                        AdminTokenExpDate = user_cred.exp_date.ToString();
                    }
                    else
                    {
                        Username = user_cred.username;
                        Name = user_cred.name;
                        Role = user_cred.user_role;
                        Token = user_cred.acces_token;
                        RefreshToken = user_cred.refresh_token;
                        TokenExpDate = user_cred.exp_date.ToString();
                    }
                    return true;
                }
                else
                {
                    if (adminPanel)
                    {
                        DeleteUserAP();
                    }
                    else
                    {
                        DeleteUser();
                    }
                    return false;
                }
            }
            return true;
        }

        public async Task<int> SignOut(bool adminPanel) // -1 - BadRequest, 1 - OK
        {
            await RefreshUserToken(adminPanel);
            var request = new RequestHandler();
            if (adminPanel)
            {
                await request.MakeRequest<string>("api/Admin/signout", AdminToken, String.Empty);
                DeleteUserAP();
            }
            else
            {
                await request.MakeRequest<string>("api/Account/signout", Token, String.Empty);
                DeleteUser();
            }
            if (request.StatusCode == HttpStatusCode.OK)
            {
                
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}