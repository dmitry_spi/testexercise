﻿using Frontend.Model;
using Newtonsoft.Json;
using Plugin.SecureStorage;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.General
{
    class AdminControl
    {
        UserManager userManager = new UserManager();

        public async Task<List<string>> GetAllUsersRoles()
        {
            var request = new RequestHandler();
            var result = await request.MakeRequest<string>("api/Admin/getroles", userManager.AdminToken, String.Empty);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                var roles_list = JsonConvert.DeserializeObject<List<string>>(result);
                return roles_list;
            }
            else
            {
                return null;
            }
        }

        public async Task<int> DeleteUser(DeleteUserRequest deleteModel) // -1 - BadRequest, 0 - Unauthorized, 1 - OK
        {
            var request = new RequestHandler();
            var result = await request.MakeRequest<DeleteUserRequest>("api/Admin/deleteuser", userManager.AdminToken, deleteModel);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                return 1;
            }
            else if (request.StatusCode == HttpStatusCode.Unauthorized)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        public async Task<List<AllUsersResponse>> GetAllUsers()
        {
            var request = new RequestHandler();
            var result = await request.MakeRequest<string>("api/Admin/getallusers", userManager.AdminToken, String.Empty);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                var users_list = JsonConvert.DeserializeObject<List<AllUsersResponse>>(result);
                return users_list;
            }
            else
            {
                return null;
            }
        }

        public async Task<int> UpdateUser(UpdateAccountRequest updateModel) // -1 - BadRequest, 0 - Unauthorized, 1 - OK
        {
            var request = new RequestHandler();
            var result = await request.MakeRequest<UpdateAccountRequest>("api/Admin/updateacc", userManager.AdminToken, updateModel);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                return 1;
            }
            else if (request.StatusCode == HttpStatusCode.Unauthorized)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        public async Task<int> NewUser(NewUserRequest userModel) // -1 - BadRequest, 0 - Unauthorized, 1 - OK
        {
            var request = new RequestHandler();
            var result = await request.MakeRequest<NewUserRequest>("api/Admin/newuser", userManager.AdminToken, userModel);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                return 1;
            }
            else if (request.StatusCode == HttpStatusCode.Unauthorized)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
    }
}
