﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.General
{
    class RequestHandler
    {
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
        public async Task<string> MakeRequest<T>(string url, string token, T model)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(Settings.ApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!String.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Add("authorization", "Bearer "+token);
                }
                var request_model = model;
                string request_string = JsonConvert.SerializeObject(request_model);
                using (HttpContent content = new StringContent(request_string, Encoding.UTF8, "application/json"))
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    response.StatusCode = System.Net.HttpStatusCode.Created;
                    try
                    {
                        response = await client.PostAsync(url, content);
                    }
                    catch(Exception ex)
                    {
                        // someting wrong
                    }
                    StatusCode = response.StatusCode;
                    if (response.StatusCode==HttpStatusCode.OK)
                    {
                        string result = await response.Content.ReadAsStringAsync();
                        response.Dispose();
                        return result;
                    }
                    else
                    {
                        response.Dispose();
                        return null;
                    }
                }
            }
        }
    }
}
