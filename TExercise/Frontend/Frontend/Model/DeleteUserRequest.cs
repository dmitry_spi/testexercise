﻿namespace Frontend.Model
{
    public class DeleteUserRequest
    {
        public string Username { get; set; }
    }
}
