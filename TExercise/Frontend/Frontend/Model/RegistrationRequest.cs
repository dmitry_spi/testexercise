﻿namespace Frontend.Model
{
    public class RegistrationRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ComfPassword { get; set; }
        public string Email { get; set; }
    }
}
