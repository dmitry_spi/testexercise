﻿namespace Frontend.Model
{
    public class TokenRefreshRequest
    {
        public string RefreshToken { get; set; }
    }
}
