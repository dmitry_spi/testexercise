﻿namespace Frontend.Model
{
    public class AllUsersResponse
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}
