﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Frontend.Model
{
    class LoginResponse
    {
        public string acces_token { get; set; }
        public string refresh_token { get; set; }
        public DateTime exp_date { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string user_role { get; set; }
    }
}
