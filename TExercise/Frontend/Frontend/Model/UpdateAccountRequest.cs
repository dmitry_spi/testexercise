﻿namespace Frontend.Model
{
    class UpdateAccountRequest
    {
        public string OldUsername { get; set; }
        public string NewUsername { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
