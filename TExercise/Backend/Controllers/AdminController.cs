﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Backend.General;
using Backend.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly HttpContext _httpContext;

        public AdminController(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _httpContext = httpContextAccessor.HttpContext;
        }

        [HttpPost("signin")]
        public async Task<IActionResult> SignIn([FromBody]UserAuth userAuth)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(userAuth.Username, userAuth.Password, true, false);
                if (result.Succeeded)
                {
                    var user = _userManager.Users.SingleOrDefault(r => r.UserName == userAuth.Username);
                    if ((await _userManager.IsInRoleAsync(user,"Admin")) || (await _userManager.IsInRoleAsync(user, "Moderator")))
                    {
                        var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
                        var r_token = TokenWorker.CreateToken(user.UserName, role, "apanel", AuthOptions.ADMIN_REFRESH_LIFETIME);
                        var now = DateTime.UtcNow;
                        var response = new
                        {
                            acces_token = TokenWorker.CreateToken(user.UserName, role, "apanel", AuthOptions.ADMIN_LIFETIME),
                            refresh_token = r_token,
                            exp_date = now.Add(TimeSpan.FromMinutes(AuthOptions.ADMIN_LIFETIME)),
                            username = user.UserName,
                            name = user.Name,
                            user_role = role
                        };
                        await _userManager.SetAuthenticationTokenAsync(user, "Bearer", "jwtadmin", r_token);
                        return Ok(response);
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [HttpPost("refreshtkn")]
        [Authorize(Policy = "APanelModerator")]
        public async Task<IActionResult> RefreshToken([FromBody]TokenRefresh tokenRefresh)
        {
            if (ModelState.IsValid)
            {
                var username = HttpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
                return await TokenWorker.RefreshToken(_userManager, tokenRefresh.RefreshToken, username, "apanel", AuthOptions.ADMIN_LIFETIME, AuthOptions.ADMIN_REFRESH_LIFETIME, "jwtadmin");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost("getroles")]
        [Authorize(Policy = "APanelModerator")]
        public IActionResult GetAllRoles()
        {
            var identityRoles = _roleManager.Roles.OrderBy(r=>r.Name).ToList();
            List<string> roles = new List<string>();
            foreach (var role in identityRoles)
            {
                roles.Add(role.Name);
            }
            return Ok(roles);
        }

        [HttpPost("deleteuser")]
        [Authorize(Policy = "APanelAdmin")]
        public async Task<IActionResult> DeleteUser([FromBody]DeleteUser deleteUser)
        {
            if (ModelState.IsValid)
            {
                User delete_user = _userManager.Users.SingleOrDefault(r => r.UserName == deleteUser.Username);
                var result = await _userManager.DeleteAsync(delete_user);
                if (result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost("getallusers")]
        [Authorize(Policy = "APanelModerator")]
        public async Task<IActionResult> GetAllUsers()
        {
            List<AllUserResponse> all_users = new List<AllUserResponse>();
            var users = _userManager.Users.OrderBy(r=>r.Name).ToList();
            foreach (var user in users)
            {
                all_users.Add(new AllUserResponse
                {
                    Username = user.UserName,
                    Name = user.Name,
                    Role = (await _userManager.GetRolesAsync(user)).FirstOrDefault()
                });
            }
            return Ok(all_users);
        }

        [HttpPost("updateacc")]
        [Authorize(Policy = "APanelModerator")]
        public async Task<IActionResult> UpdateAccount([FromBody]UserUpdate userUpdate)
        {
            if (ModelState.IsValid)
            {
                var user_update = userUpdate;
                var username = HttpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
                User user = _userManager.Users.SingleOrDefault(r => r.UserName == username);
                if (await _userManager.IsInRoleAsync(user, "Moderator") && user.UserName == user_update.OldUsername)
                {
                    user_update.Role = "Moderator";
                    var succes = await UpdateUser(user_update, _userManager);
                    if (succes)
                    {
                        return Ok();
                    }
                }
                else if (await _userManager.IsInRoleAsync(user, "Admin"))
                {
                    var succes = await UpdateUser(user_update, _userManager);
                    if (succes)
                    {
                        return Ok();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [HttpPost("newuser")]
        [Authorize(Policy = "APanelAdmin")]
        public async Task<IActionResult> CreateUser([FromBody]UserCreate userCreate)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    UserName = userCreate.Username,
                    Name = userCreate.Name,
                    Email = userCreate.Email
                };
                var result = await _userManager.CreateAsync(user, userCreate.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, userCreate.Role);
                    return Ok();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [HttpPost("signout")]
        [Authorize(Policy = "APanelModerator")]
        public async Task<IActionResult> SignOut()
        {
            var username = _httpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            User user = _userManager.Users.SingleOrDefault(r => r.UserName == username);
            var result = await _userManager.RemoveAuthenticationTokenAsync(user, "Bearer", "jwtadmin");
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        private static async Task<bool> UpdateUser(UserUpdate userUpdate, UserManager<User> userManager)
        {
            User user_update = userManager.Users.SingleOrDefault(r => r.UserName == userUpdate.OldUsername);
            // set new params for user
            user_update.UserName = userUpdate.NewUsername;
            user_update.Name = userUpdate.Name;
            var result = await userManager.UpdateAsync(user_update);
            if (!result.Succeeded)
            {
                return false;
            }
            User user = userManager.Users.SingleOrDefault(r => r.UserName == userUpdate.NewUsername);
            // set new password
            if (userUpdate.Password != null)
            {
                user.PasswordHash = userManager.PasswordHasher.HashPassword(user, userUpdate.Password);
                result = await userManager.UpdateAsync(user);
                if (!result.Succeeded)
                {
                    return false;
                }
                user = userManager.Users.SingleOrDefault(r => r.UserName == userUpdate.NewUsername);
            }
            // update role
            var current_role = await userManager.GetRolesAsync(user);
            result = await userManager.RemoveFromRolesAsync(user, current_role);
            if (!result.Succeeded)
            {
                return false;
            }
            result = await userManager.AddToRoleAsync(user, userUpdate.Role);
            if (!result.Succeeded)
            {
                return false;
            }
            return true;
        }
    }

    public class AllUserResponse
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}