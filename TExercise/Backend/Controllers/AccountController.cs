﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Backend.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Backend.General;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly HttpContext _httpContext;
        
        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _httpContext = httpContextAccessor.HttpContext;
        }

        // Comment it if you are using Initializer in the Main method
        [HttpGet("init")]
        public async Task<string> Init()
        {
            await UserInitializer.Initialize(_userManager, _roleManager);
            return "OK";
        }

        [HttpPost("registration")]
        public async Task<IActionResult> Registration([FromBody]UserRegister model)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    UserName = model.Username,
                    Email = model.Email,
                    Name = model.Username
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "User");
                    await _signInManager.SignInAsync(user, false);
                    var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
                    var r_token = TokenWorker.CreateToken(user.UserName, role, "general", AuthOptions.REFRESH_LIFETIME);
                    var now = DateTime.UtcNow;
                    var response = new
                    {
                        acces_token = TokenWorker.CreateToken(user.UserName, role, "general", AuthOptions.LIFETIME),
                        refresh_token = r_token,
                        exp_date = now.Add(TimeSpan.FromDays(AuthOptions.LIFETIME)),
                        username = user.UserName,
                        name = user.Name,
                        user_role = role
                    };
                    await _userManager.SetAuthenticationTokenAsync(user, "Bearer", "jwt", r_token);
                    return Ok(response);
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
            return BadRequest();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]UserAuth model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, true, false);
                if (result.Succeeded)
                {
                    var user = _userManager.Users.SingleOrDefault(r => r.UserName == model.Username);
                    var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
                    var r_token = TokenWorker.CreateToken(user.UserName, role, "general", AuthOptions.REFRESH_LIFETIME);
                    var now = DateTime.UtcNow;
                    var response = new
                    {
                        acces_token = TokenWorker.CreateToken(user.UserName, role, "general", AuthOptions.LIFETIME),
                        refresh_token = r_token,
                        exp_date = now.Add(TimeSpan.FromDays(AuthOptions.LIFETIME)),
                        username = user.UserName,
                        name = user.Name,
                        user_role = role
                    };
                    await _userManager.SetAuthenticationTokenAsync(user, "Bearer", "jwt", r_token);
                    return Ok(response);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost("refreshtoken")]
        [Authorize(Policy = "AnyRequire")]
        public async Task<IActionResult> RefreshToken([FromBody]TokenRefresh tokenRefresh)
        {
            if (ModelState.IsValid)
            {
                var username = HttpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
                return await TokenWorker.RefreshToken(_userManager, tokenRefresh.RefreshToken, username, "apanel", AuthOptions.LIFETIME, AuthOptions.REFRESH_LIFETIME, "jwt");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPost("signout")]
        [Authorize(Policy = "AnyRequire")]
        public async Task<IActionResult> SignOut()
        {
            var username = _httpContext.User.Claims.SingleOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            User user = _userManager.Users.SingleOrDefault(r => r.UserName == username);
            var result = await _userManager.RemoveAuthenticationTokenAsync(user, "Bearer", "jwt");
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}