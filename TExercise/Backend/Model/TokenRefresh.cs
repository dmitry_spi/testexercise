﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Model
{
    public class TokenRefresh
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}
