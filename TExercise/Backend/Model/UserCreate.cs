﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Model
{
    public class UserCreate
    {
        [Required(ErrorMessage = "Username must be set")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Name must be set")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Password must be set")]
        public string Password { get; set; }
        public string Email { get; set; }
        [Required(ErrorMessage = "Role must be set")]
        public string Role { get; set; }
    }
}
