﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Model
{
    public class UserUpdate
    {
        [Required(ErrorMessage = "Username must be set")]
        public string OldUsername { get; set; }
        public string NewUsername { get; set; }
        [Required(ErrorMessage = "Name must be set")]
        public string Name { get; set; }
        public string Password { get; set; }
        [Required(ErrorMessage = "Role must be set")]
        public string Role { get; set; }
    }
}
