﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Model
{
    public class UserRegister
    {
        [Required(ErrorMessage ="Username must be set")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password must be set")]
        public string Password { get; set; }
        [Compare("Password",ErrorMessage ="Passwords do not match")]
        public string ComfPassword { get; set; }
        [Required(ErrorMessage = "E-mail must be set")]
        public string Email { get; set; }
    }
}
