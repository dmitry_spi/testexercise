﻿using Microsoft.AspNetCore.Identity;

namespace Backend.Model
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
    }
}
