﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Backend.General
{
    public class AuthOptions
    {
        public const string ISSUER = "TestExercise";
        public const string AUDIENCE = "https://localhost/";
        const string KEY = "superkeysuperkey8";
        public const int LIFETIME = 60; // lifetime in minuts
        public const int REFRESH_LIFETIME = 60*24*7; // lifetime in minutes
        public const int ADMIN_LIFETIME = 10; // lifetime in minuts
        public const int ADMIN_REFRESH_LIFETIME = 60; // lifetime in minuts
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
