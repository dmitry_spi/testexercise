﻿using Backend.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend.General
{
    public class TokenWorker
    {
        public static async Task<IActionResult> RefreshToken(UserManager<User> userManager, string refreshToken, string username, string site, int lifetime, int refreshlifetime, string tokenName)
        {
            var user = userManager.Users.SingleOrDefault(r => r.UserName == username);
            var true_token = await userManager.GetAuthenticationTokenAsync(user, "Bearer", tokenName);
            if (true_token.CompareTo(refreshToken)==0)
            {
                var role = (await userManager.GetRolesAsync(user)).FirstOrDefault();
                var r_token = CreateToken(user.UserName, role, site, refreshlifetime);
                var now = DateTime.UtcNow;
                var response = new
                {
                    acces_token = CreateToken(user.UserName, role, site, lifetime),
                    refresh_token = r_token,
                    exp_date = now.Add(TimeSpan.FromMinutes(lifetime)),
                    username = user.UserName,
                    name = user.Name,
                    user_role = role
                };
                await userManager.SetAuthenticationTokenAsync(user, "Bearer", tokenName, r_token);
                return new OkObjectResult(response);
            }
            else
            {
                return new UnauthorizedResult();
            }
        }

        public static string CreateToken(string username, string role, string type, int lifetime)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: GetClaimsIdentity(username, role, type).Claims,
                    expires: now.Add(TimeSpan.FromMinutes(lifetime)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);
            return token;
        }

        private static ClaimsIdentity GetClaimsIdentity(string username, string role, string type)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, username),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                new Claim("type",type)
            };
            return new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
        }
    }
}
