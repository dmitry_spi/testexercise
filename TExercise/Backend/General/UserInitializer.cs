﻿using Backend.Model;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.General
{
    public class UserInitializer
    {
        public static async Task Initialize(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.Roles.Any())
            {
                await roleManager.CreateAsync(new IdentityRole("Admin"));
                await roleManager.CreateAsync(new IdentityRole("Moderator"));
                await roleManager.CreateAsync(new IdentityRole("User"));
            }
            if (!userManager.Users.Any())
            {
                User admin = new User
                {
                    UserName = "admin",
                    Name = "AdminName"
                };
                User moderator = new User
                {
                    UserName = "moder",
                    Name = "ModerName"
                };
                User user = new User
                {
                    UserName = "user",
                    Name = "UserName",
                    Email = "user@mail.com",
                    EmailConfirmed = true
                };
                await userManager.CreateAsync(admin, "admin");
                await userManager.CreateAsync(moderator, "moderator");
                await userManager.CreateAsync(user, "user");
                await userManager.AddToRoleAsync(admin, "Admin");
                await userManager.AddToRoleAsync(moderator, "Moderator");
                await userManager.AddToRoleAsync(user, "User");
            }
        }
    }
}
